#ifndef PERSONNAGE_H
#define PERSONNAGE_H
#include "Inventaire.h"
#include "Equipement.h"
#include <iostream>
using namespace std;

class Personnage : public Inventaire, public Equipement
{
    public:
        Personnage(string);
        virtual ~Personnage();
        void infos();
        string getNomPersonnage(){return nomPerso;}
        void getInventaire();
        void setInitProfil();


        void getCompetences();
        void setForce(int);
        void setIntelligence(int);
        void setRuse(int);
        void setDexterite(int);
        void setConstitution(int);
        void setEloquence(int);

        void setCrochetage(int);
        void setDetroussement(int);
        void setEscalade(int);
        void setOrientation(int);
        void setDissimulation(int);
        void setPistage(int);
        void setCommandement(int);
        void setPremierSoins(int);
        void selectmetier(int);
        void selectRace(int);

    protected:

    private:
        string nomPerso;
        Inventaire* monInventaire;
        int PV;
        int pointDeMagie;


        //metier
        string metier;
        //classe de personnages (modificateur de  metier et modificateur de race
        // caractéristique compétence

        int Force=1; //: permet d'utilier des armes de plus en plus lourdes
        int Intelligence=1; //: permet de lancer des sorts
        int Ruse=1; //: permet de déverouiller des serrures et autres compétences de ce genre
        int Dexterite=1; //: permet de porter des coups avec plus d'efficacité
        int Constitution=1; //: permet de porter des armures plus lourdes
        int Eloquence=1; //: permet certains rapports sociaux
        int Crochetage=1;
        int Detroussement=1;
        int Escalade=1;
        int Orientation=1;
        int Dissimulation=1;
        int Pistage=1;
        int Commandement=1;

        int PremierSoins=1;





};

#endif // PERSONNAGE_H
