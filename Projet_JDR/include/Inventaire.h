#ifndef INVENTAIRE_H
#define INVENTAIRE_H
#define ITEMMAX 10
#include <Item.h>
#include <iostream>
#include <map>
class Inventaire
{
    public:
        Inventaire();
        virtual ~Inventaire();
        void ajouterItem(Item*);
        void suppressionItem(int);
        bool itemPresent (int);

    protected:

    private:
        map<int, Item *> mesItems;
};

#endif // INVENTAIRE_H
