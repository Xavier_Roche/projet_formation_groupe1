#ifndef EQUIPEMENT_H
#define EQUIPEMENT_H
#include <iostream>
#include <string>
using namespace std;

class Equipement
{
    public:
        Equipement();
        virtual ~Equipement();

        struct weapon
        {
            string weaponName;
            int weaponDamage;
            int weaponRange;
        };

        struct armor
        {
            string armorName;
            int armorValue;
        };

        weapon epee;
        weapon masse;
        weapon dague;
        weapon lance;
        weapon arc;

        armor armortissu;
        armor armorcuir;
        armor armorplate;
        armor armormailles;

    protected:

    private:
};


#endif // EQUIPEMENT_H
