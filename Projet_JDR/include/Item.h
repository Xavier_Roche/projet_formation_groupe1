#ifndef ITEM_H
#define ITEM_H
#include <string>
using namespace std;
class Item
{
    public:
        Item();
        virtual ~Item();
        int Getnumero() { return numero; }
        void Setnumero(int val) { numero = val; }
        string GetNomItem() { return designation; }
        void SetNomItem(string val) { designation = val; }
    protected:

    private:
        int numero;
        string designation;
};

#endif // ITEM_H
