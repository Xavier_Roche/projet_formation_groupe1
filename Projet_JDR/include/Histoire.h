#ifndef HISTOIRE_H
#define HISTOIRE_H
#include "split.h"
#include "Inventaire.h"
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>



class Histoire
{
    public:
        Histoire();
        virtual ~Histoire();
        void lectureFichierHistoire(int,Inventaire);
        int getprochainFichierALire(int);

    protected:

    private:
        int prochainFichierALireChoix1;
        int prochainFichierALireChoix2;
        int choix_histoire;

};

#endif // HISTOIRE_H
