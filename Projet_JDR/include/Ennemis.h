#include <string>

using namespace std;

class Ennemis
{
public:
    Ennemis();
    ~Ennemis();

    struct ennemis
    {
        string ennemisNom;
        int maxPV;
        int ennemisAttaque;
        int ennemisDefense;
        int pv = maxPV;
        int ennemisPortee;
    };

    ennemis gobelins;
    ennemis gardes;
    ennemis bandit;
    ennemis banditarcher;
    ennemis araignee;
    ennemis mage;
};
