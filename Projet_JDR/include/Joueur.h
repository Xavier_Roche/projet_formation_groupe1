#ifndef JOUEUR_H
#define JOUEUR_H
#define NB_Perso 30
#include <string>
#include <iostream>
#include "Personnage.h"
#include "Histoire.h"

using namespace std;

class Joueur
{
    public:
        Joueur(string);
        virtual ~Joueur();
        Personnage* getPersonnage();
        void setPersonnage(Personnage* P);


        void setNomJoueur(string);
        string getNomJoueur();

    protected:

    private:
        string nomJoueur;
        int slotDePersonnage=0;
        Personnage* Nouveau_Perso[NB_Perso];
        Histoire* Nouvelle_histoire;
};

#endif // JOUEUR_H
