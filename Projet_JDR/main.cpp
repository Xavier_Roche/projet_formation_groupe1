#include <iostream>
#include <Joueur.h>
#include <Personnage.h>
#include <Histoire.h>

using namespace std;

int main()
{
    string nomjoueur;
    string nomperso;
    int choix;
    Inventaire moninventaire;

    //selection joueur et personnage
    cout << "Bienvenu dans notre JDR" << endl;

    cout << "Quel est votre nom?" << endl;
    cin >> nomjoueur;
    Joueur joueur1(nomjoueur);
    cout << "le nom du joueur est "<<joueur1.getNomJoueur()<<endl;

    cout << "Veuillez creer un nouveau personnage, quel est son nom?" << endl;
    cin >> nomperso;

    Personnage perso1(nomperso);
    perso1.getCompetences();


    //debut histoire
    Histoire histoire1;
    histoire1.lectureFichierHistoire(1,moninventaire);
    cout << "\nle premier txt est lu" << endl;
    histoire1.lectureFichierHistoire(histoire1.getprochainFichierALire(1),moninventaire);
    while (1)
        {

            cout << "\nle second txt est lu quel est votre choix (1 ou 2)?" << endl;
            cin>>choix;
            histoire1.lectureFichierHistoire(histoire1.getprochainFichierALire(choix),moninventaire);
        }
    return 0;
}
