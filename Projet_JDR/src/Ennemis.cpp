#include "Ennemis.h"

struct ennemis
{
    string ennemisNom;
    int maxPV;
    int ennemisAttaque;
    int ennemisDefense;
    int ennemisPortee;
};

Ennemis::Ennemis()
{
    //gobelin
    gobelins.ennemisNom = "Gobelin";
    gobelins.maxPV = 10;
    gobelins.ennemisAttaque = 3;
    gobelins.ennemisDefense = 1;
    gobelins.ennemisPortee = 1;

    //gardes
    gardes.ennemisNom = "Garde";
    gardes.maxPV = 20;
    gardes.ennemisAttaque = 5;
    gardes.ennemisDefense = 4;
    gardes.ennemisPortee = 2;

    //bandits
    bandit.ennemisNom = "Bandit";
    bandit.maxPV = 18;
    bandit.ennemisAttaque = 4;
    bandit.ennemisDefense = 1;
    bandit.ennemisPortee = 2;

    //banditarcher
    banditarcher.ennemisNom = "Bandit archer";
    banditarcher.maxPV = 18;
    banditarcher.ennemisAttaque = 2;
    banditarcher.ennemisDefense = 1;
    banditarcher.ennemisPortee = 15;

    //araignee
    araignee.ennemisNom = "Araignee";
    araignee.maxPV = 25;
    araignee.ennemisAttaque = 3;
    araignee.ennemisDefense = 2;
    araignee.ennemisPortee = 1;

    //mage
    mage.ennemisNom = "Magicien";
    mage.maxPV = 15;
    mage.ennemisAttaque = 8;
    mage.ennemisDefense = 0;
    mage.ennemisPortee = 15;
}

