#include "Equipement.h"

Equipement::Equipement()
{

    epee.weaponName = "Epee";
    masse.weaponName = "Masse";
    dague.weaponName = "dague";
    lance.weaponName = "Lance";
    arc.weaponName = "Arc";

    epee.weaponDamage = 5;
    masse.weaponDamage = 4;
    dague.weaponDamage = 3;
    lance.weaponDamage = 3;
    arc.weaponDamage = 2;

    epee.weaponRange = 2;
    masse.weaponRange = 2;
    dague.weaponRange = 1;
    lance.weaponRange = 4;
    arc.weaponRange = 15;

    ///////////////////////////////
    ////////ARMURE/////////////////
    ///////////////////////////////

    armortissu.armorName = "Tenue en tissu";
    armorcuir.armorName = "Armure de cuir";
    armormailles.armorName = "Armure de mailles";
    armorplate.armorName = "Armure de plaques";

    armortissu.armorValue = 0;
    armorcuir.armorValue = 1;
    armormailles.armorValue = 2;
    armorplate.armorValue = 4;
}

Equipement::~Equipement()
{
    //dtor
}
