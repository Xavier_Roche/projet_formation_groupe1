#include "Personnage.h"

Personnage::Personnage(string NomDePersonnage)
{
    nomPerso=NomDePersonnage;
    cout<<"Construction du personnage"<< nomPerso<<endl;
    setInitProfil();
    PV=100;
    pointDeMagie=50;

    //ctor
}

Personnage::~Personnage()
{
    cout<<"Destruction d'un Personnage"<<endl;

    //dtor
}
void Personnage::infos()
{
    cout<<"Je suis le personnage" << nomPerso<<endl;
    cout<<"ma force est de " << Force<<endl;
    cout<<"Mes PV sont a" << PV<<endl;
    cout<<"mes point de magie sont a" << pointDeMagie<<endl;
}
void Personnage::getInventaire()
{

}
void Personnage::setInitProfil()
{
    int metier;
    int race;
    cout << "selection du metier dans la liste"<< endl;
    cout << "1: guerrier, 2:mage, 3:voleur"<<endl;
    cin >> metier;
    selectmetier(metier);
    cout << "selection de la race dans la liste"<< endl;
    cout << "1: humain, 2:orc, 3:elfe"<<endl;
    cin >> race;
    selectRace(race);
    //switch

}
void Personnage::getCompetences()
{
    cout<< " La force du personnage est  de "<<Force<<endl;
    cout<< " La Intelligence du personnage est  de "<<Intelligence<<endl;
    cout<< " La Ruse du personnage est  de "<<Ruse<<endl;
    cout<< " La Dexterite du personnage est  de "<<Dexterite<<endl;
    cout<< " La Constitution du personnage est  de "<<Constitution<<endl;
    cout<< " La Eloquence du personnage est  de "<<Eloquence<<endl;
    cout<< " La Crochetage du personnage est  de "<<Crochetage<<endl;
    cout<< " La Detroussement du personnage est  de "<<Detroussement<<endl;
    cout<< " La Escalade du personnage est  de "<<Escalade<<endl;
    cout<< " La Orientation du personnage est  de "<<Orientation<<endl;
    cout<< " La Dissimulation du personnage est  de "<<Dissimulation<<endl;
    cout<< " La Pistage du personnage est  de "<<Pistage<<endl;
    cout<< " La Commandement du personnage est  de "<<Commandement<<endl;
    cout<< " La PremierSoins du personnage est  de "<<PremierSoins<<endl;
}
void Personnage::selectmetier(int metier_choisi)
{

    switch(metier_choisi) {
        case 1:
            {
                std::cout << "Le guerrier ete selectionne\n";
                Force = Force + 3;
                Constitution = Constitution + 3;
                Intelligence = Intelligence - 1;
                Commandement = Commandement + 2;
                Dissimulation = Dissimulation - 2;
                Escalade = Escalade + 2;
                break;
            }

        case 2:
            {
                std::cout << "Le mage ete selectionne\n";
                Intelligence = Intelligence + 3;
                Constitution = Constitution + 1;
                Force = Force - 1;
                Orientation = Orientation + 1;
                PremierSoins = PremierSoins +1;
                break;
            }
        case 3:
            {
                std::cout << "Le voleur ete selectionne\n";

                Constitution = Constitution + 1;

                Dissimulation = Dissimulation + 3;
                Escalade = Escalade + 3;
                Detroussement = Detroussement +3;
                Crochetage = Crochetage + 3;
                break;
            }
        default: cout<<"le metier selectionne n'est pas valide"<<endl; break;
    }

}
void Personnage::selectRace(int race_choisi)
{

    switch(race_choisi) {
        case 1:
            {
                std::cout << "L'humain a ete selectionne\n";
                Force = Force + 1;
                Constitution = Constitution + 1;
                Intelligence = Intelligence + 2;
                Commandement = Commandement + 2;

                break;
            }

        case 2:
            {
                std::cout << "L'orc a ete selectionne\n";

                Constitution = Constitution + 3;
                Force = Force + 3;


                break;
            }
        case 3:
            {
                std::cout << "L'elfe a ete selectionne\n";

                Constitution = Constitution + 1;
                Intelligence = Intelligence + 2;
                Dissimulation = Dissimulation + 3;
                Escalade = Escalade + 3;
                Detroussement = Detroussement +3;
                Crochetage = Crochetage + 3;
                break;
            }
        default: cout<<"le metier selectionne n'est pas valide"<<endl; break;
    }
}


void Personnage::setForce(int competence)
{
    Force=competence;
}

void Personnage::setIntelligence(int competence)
{
    Intelligence=competence;
}

void Personnage::setRuse(int competence)
{
Ruse=competence;
}

void Personnage::setDexterite(int competence)
{
Dexterite=competence;
}

void Personnage::setConstitution(int competence)
{
Constitution=competence;
}

void Personnage::setEloquence(int competence)
{
Eloquence=competence;
}

void Personnage::setCrochetage(int competence)
{
Crochetage=competence;
}

void Personnage::setDetroussement(int competence)
{
Detroussement=competence;
}

void Personnage::setEscalade(int competence)
{
Escalade=competence;
}

void Personnage::setOrientation(int competence)
{
Orientation=competence;
}

void Personnage::setDissimulation(int competence)
{
Dissimulation=competence;
}

void Personnage::setPistage(int competence)
{
Pistage=competence;
}

void Personnage::setCommandement(int competence)
{
Commandement=competence;
}

void Personnage::setPremierSoins(int competence)
{
PremierSoins=competence;
}
